﻿using System;

namespace comp5002_10001681_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {
             //Print welcome message to screen
            Console.Clear();
            Console.WriteLine("****** Welcome to my shop! ******");
            Console.WriteLine();
            
            //Declare all variables
            var nameFirst = "";
            var price1 = 0.0;
            var answer1 = true;
            var price2 = 0.0;
            var gst = 0.15;
            
            //Ask user to input first name, assign to var nameFirst 
            Console.WriteLine("Please enter your First name?");
            nameFirst = Console.ReadLine();
            
            //Ask user to input double and assign to var price1
            Console.WriteLine($"Thanks {nameFirst}! Please enter the price of your first item to 2 decimal places. Eg. 4.00");
            price1 = double.Parse(Console.ReadLine());
            

            //Ask user if they want to input additional double - Yes/No
            Console.WriteLine("Would you like to enter an additional item price? (True=Yes/False=No)");
            answer1 = bool.Parse(Console.ReadLine());
            
            /*if user input answer is True then ask the user to enter a new double. Store double as var price2.
            Calculate the sum of var price1 and price2 - Replace price1 value with the sum of price1/price2*/
            if (answer1)
        {   Console.WriteLine("Please enter the price of your second item to 2 decimal places. EG. 5.89");
            price2 = double.Parse(Console.ReadLine());
            Console.WriteLine($"The total cost of your items excluding GST = {price1+=price2}");
            }
            //if user input is False display the total of price1 to screen
            if(!answer1)
        {   Console.WriteLine($"The total cost of your item/s excluding GST = {price1}");
            
            }
            //Display to screen the total sum of (price1 x gst)+price1 (total cost including gst(15%))
            Console.WriteLine();
            Console.WriteLine($"The total cost of your item/s including GST = {price1*gst+price1}");
            Console.WriteLine();
            //Display to screen thank you message
            Console.WriteLine($"Thank you for shopping with us {nameFirst}, please come again :) !");
            Console.WriteLine();
            Console.WriteLine("Press <ENTER> key to exit program");
            Console.ReadKey();
        }
    }
}
